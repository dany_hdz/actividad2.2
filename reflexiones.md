Daniela Hernández y Hernández   A01730397
09/03/2020

## Actividad 2.2: programación funcional pt.2
## REFLEXIÓN

Para esta actividad, las estructuras de datos que seleccioné para representar el grafo
de la red de petri fueron pares ordenados y presets/posets.
En el caso de los pares ordenados, la formé como una lista de listas, ya que me parece 
una de las formas más concisas y sencillas, tanto para escribirla, como para leerla y 
entenderla. Además de que es con la estructura que más me siento cómoda manejando en 
Elixir, pues es la que más hemos usado en ejercicios anteriores.
Mi segunda representación decidí implementarla como un mapa de mapas, en donde la llave
'principal' es el nodo, y el valor es un mapa con sus presets y posets. Apesar de que 
esta estructura es un poco más compleja y larga para escribir, la elegí porque permite
organizar de manera muy clara cada nodo.

En cuando a la función de firing, la estructura de mapas, es decir, presets y posets,
tenía como ventaja que puedes acceder de manera directa a estos conjuntos de cada nodo.
En mi caso, usé la función Map.get para acceder a estos valores porque me parecía más
comprensible dentro de mi lógica de programación, sin embargo esto podría optimizarse
con un Reduce.
Por parte de los pares ordenados, el procedimiento era un poco más laborioso, ya que 
debía construir los presets o posets para el nodo en cuestión. No obstante, esta no es
una tarea muy complicada, por lo que escribí unas funciones auxiliares que revisaran 
de manera recursiva cada par ordenado para obtener sus sets.
A partir de ahí, la lógica para ambas estructuras es básicamente la misma, puesto que
checo que el preset sea un subconjunto del marcado actual, si es así, actualizo el 
marcado, de lo contrario, no se modifica nada.

