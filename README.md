# ACTIVIDAD 2.2: Programación funcional

Daniela Hernández y Hernández   A01730397
08/03/2021

Representaciones de Red de Petri: 
% Pares ordenados
% Mapa de presets y posets

- Pregunta 1: firing/3
- Pregunta 2: enablement/2
- Pregunta 3: replay/3
- Pregunta 4: reachability_graph/3



## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `grafos_h3` to your list of dependencies in `mix.exs`:

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/grafos_h3](https://hexdocs.pm/grafos_h3).

