defmodule GrafosV1 do
  # PREGUNTA 1
  # -- Representación de la red de Petri (Grafo) -----------------------

  # Lista de de pares ordenados -> Lista de listas
  @paresOrd [
    ["p0", "A"],
    ["A", "p1"], ["A", "p2"],
    ["p1", "B"], ["p1", "D"],
    ["p2", "C"], ["p2", "D"],
    ["B", "p3"],
    ["C", "p4"],
    ["D", "p3"], ["D", "p4"],
    ["p3", "E"],
    ["p4", "E"],
    ["E", "p5"]
  ]

  @paresOrd2 [
    ["p0", "A"],
    ["A", "p1"], ["A", "p2"],
    ["p1", "B"],
    ["p2", "C"],
    ["B", "p3"],
    ["C", "p4"],
    ["D", "p2"],
    ["p3", "E"],
    ["p4", "D"], ["p4", "E"],
    ["E", "p5"]
  ]

  @paresOrd3 [
    ["p0", "A"],
    ["A", "p1"], ["A", "p2"],
    ["p1", "B"], ["p1", "D"],
    ["p2", "C"], ["p2", "D"],
    ["B", "p3"],
    ["C", "p4"],
    ["D", "p3"],
    ["p3", "E"],
    ["p4", "E"],
    ["E", "p5"]
  ]

  def pares1(), do: @paresOrd
  def pares2(), do: @paresOrd2
  def pares3(), do: @paresOrd3


  # -- Función firing -------------------------------------------------
  def getPreset([], _letter), do: []
  def getPreset([a|b], letter) do
    if tl(a) == [letter], do: [hd(a)] ++ getPreset(b, letter),
    else: getPreset(b, letter)
  end

  def getPoset([], _letter), do: []
  def getPoset([a|b], letter) do
    if hd(a) == letter, do: tl(a) ++ getPoset(b, letter),
    else: getPoset(b, letter)
  end

  def firing(petri, marcado, trans) do
    preset = MapSet.new(getPreset(petri, trans))
    if MapSet.subset?(preset, MapSet.new(marcado)) do
      list = marcado -- MapSet.to_list(preset)
      Enum.sort(list ++ getPoset(petri, trans))
    else
       marcado
    end
  end

  # PREGUNTA 2
  # -- Función enablement -------------------------------------------------
  # Regresa transiciones posibles
  def enable1(_petri, []), do: []
  def enable1(petri, [a|b]) do
    getPoset(petri, a) ++ enable1(petri, b)
  end

  # Regresa transiciones filtradas
  def enable2(_petri, [], _m), do: []
  def enable2(petri, [a|b], m) do
    preset = getPreset(petri, a) |> MapSet.new
    if MapSet.subset?(preset, MapSet.new(m)), do: [a] ++ enable2(petri, b, m),
    else: enable2(petri, b, m)
  end

  def enablement(petri, marcado) do
    possible = enable1(petri, marcado) |> Enum.uniq
    enable2(petri, possible, marcado)
  end

  # PREGUNTA 3
  # -- Función replay -------------------------------------------------

  def line(_petri, "", _m), do: true
  def line(petri, a, m) do
    result = firing(petri, m, String.first(a))
    if result != m, do: line(petri, String.slice(a, 2..-1), result), else: false
  end

  def play(_petri, [], _m), do: 0
  def play(petri, [a|b], m) do
    if line(petri, a, m), do: 1 + play(petri,b,m), else: 0 + play(petri,b,m)
  end

  def replay(petri, marcado, archivo) do
    total = File.read!(archivo) |> String.split
    pass = play(petri, total, marcado)
    fail = length(total) - pass
    [pass] ++ [fail]
  end

  # PREGUNTA 4
  # -- Grafo de alcance -------------------------------------------------

  def reachability_graph(petri, marcado, graph) do
    enablement(petri, marcado)
    |> Enum.reduce(graph, fn x, graph ->
      fire = firing(petri, marcado, x)
      node = [marcado, x, fire]
      if !Enum.member?(graph, node) do
        update = graph ++ [node]
        reachability_graph(petri, fire, update)
      else
        graph
      end
    end)
  end

end

################################################################################

defmodule GrafosV2 do
  # PREGUNTA 1
  # -- Representación de la red de Petri (Grafo) -----------------------

  # Lista presets y posets -> map de maps
  @prePosets1 %{
    p0: %{"pre" => [], "pos" => ["A"]},
    A: %{"pre" => ["p0"], "pos" => ["p1", "p2"]},
    p1: %{"pre" => ["A"], "pos" => ["B", "D"]},
    p2: %{"pre" => ["A"], "pos" => ["C", "D"]},
    B: %{"pre" => ["p1"], "pos" => ["p3"]},
    C: %{"pre" => ["p2"], "pos" => ["p4"]},
    D: %{"pre" => ["p1", "p2"], "pos" => ["p3", "p4"]},
    p3: %{"pre" => ["B", "D"], "pos" => ["E"]},
    p4: %{"pre" => ["C", "D"], "pos" => ["E"]},
    E: %{"pre" => ["p3", "p4"], "pos" => ["p5"]},
    p5: %{"pre" => ["E"], "pos" => []}
  }

  @prePosets2 %{
    p0: %{"pre" => [], "pos" => ["A"]},
    A: %{"pre" => ["p0"], "pos" => ["p1", "p2"]},
    p1: %{"pre" => ["A"], "pos" => ["B"]},
    p2: %{"pre" => ["A"], "pos" => ["C"]},
    B: %{"pre" => ["p1"], "pos" => ["p3"]},
    C: %{"pre" => ["p2"], "pos" => ["p4"]},
    D: %{"pre" => ["p4"], "pos" => ["p2"]},
    p3: %{"pre" => ["B"], "pos" => ["E"]},
    p4: %{"pre" => ["C"], "pos" => ["D","E"]},
    E: %{"pre" => ["p3", "p4"], "pos" => ["p5"]},
    p5: %{"pre" => ["E"], "pos" => []}
  }

  @prePosets3 %{
    p0: %{"pre" => [], "pos" => ["A"]},
    A: %{"pre" => ["p0"], "pos" => ["p1", "p2"]},
    p1: %{"pre" => ["A"], "pos" => ["B", "D"]},
    p2: %{"pre" => ["A"], "pos" => ["C", "D"]},
    B: %{"pre" => ["p1"], "pos" => ["p3"]},
    C: %{"pre" => ["p2"], "pos" => ["p4"]},
    D: %{"pre" => ["p1", "p2"], "pos" => ["p3"]},
    p3: %{"pre" => ["B", "D"], "pos" => ["E"]},
    p4: %{"pre" => ["C"], "pos" => ["E"]},
    E: %{"pre" => ["p3", "p4"], "pos" => ["p5"]},
    p5: %{"pre" => ["E"], "pos" => []}
  }

  def sets1(), do: @prePosets1
  def sets2(), do: @prePosets2
  def sets3(), do: @prePosets3


  # -- Función firing -------------------------------------------------
  # marcado debe ser una lista de strings y trans el nombre de una transición como átomo
  def firing(red, marcado, trans) do
    preset = MapSet.new(Map.get(Map.get(red, trans), "pre"))
    if MapSet.subset?(preset, MapSet.new(marcado)) do
      list = marcado -- MapSet.to_list(preset)
      rest = Map.get(red, trans) |> Map.get("pos")
      Enum.sort(list ++ rest)
    else
        marcado
    end
  end

  # PREGUNTA 2
  # -- Función enablement -------------------------------------------------
  def enable1(_red, []), do: []
  def enable1(red, [a|b]) do
    possible = Map.get(red, a) |> Map.get("pos")
    possible ++ enable1(red, b)
  end

  def enable2(_red, [], _m), do: []
  def enable2(red, [a|b], m) do
    preset = Map.get(red, String.to_atom(a)) |> Map.get("pre")
    |> Enum.map(fn(x) -> String.to_atom(x) end) |> MapSet.new
    if MapSet.subset?(preset, MapSet.new(m)), do: [a] ++ enable2(red, b, m),
    else: enable2(red, b, m)
  end

  # El marcado es una lista de átomos
  def enablement(_red, []), do: []
  def enablement(red, marcado) do
    possible = enable1(red, marcado) |> Enum.uniq
    enable2(red, possible, marcado)
  end

  # PREGUNTA 3
  # -- Función replay -------------------------------------------------
  def line(_red, "", _m), do: true
  def line(red, a, m) do
    trans = String.first(a) |> String.to_atom
    result = firing(red, m, trans)
    if result != m, do: line(red, String.slice(a, 2..-1), result), else: false
  end

  def play(_red, [], _m), do: 0
  def play(red, [a|b], m) do
    if line(red, a, m), do: 1 + play(red,b,m), else: 0 + play(red,b,m)
  end

  def replay(red, marcado, archivo) do
    total = File.read!(archivo) |> String.split
    pass = play(red, total, marcado)
    fail = length(total) - pass
    [pass] ++ [fail]
  end

  # PREGUNTA 4
  # -- Grafo de alcance -------------------------------------------------

  def reachability_graph(red, marcado, graph) do
    markStr = Enum.map(marcado, fn(x) -> String.to_atom(x) end)
    enablement(red, markStr)
    |> Enum.reduce(graph, fn x, graph ->
      fire = firing(red, marcado, String.to_atom(x))
      node = [marcado, x, fire]
      if !Enum.member?(graph, node) do
        update = graph ++ [node]
        reachability_graph(red, fire, update)
      else
        graph
      end
    end)
  end

end
