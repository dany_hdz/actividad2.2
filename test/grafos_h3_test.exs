defmodule GrafosV1Test do
  use ExUnit.Case
  doctest GrafosV1

  test "Pregunta 1: firing con pares ordenados" do
    red = GrafosV1.pares1
    marcado = ["p0"]
    assert GrafosV1.firing(red, marcado, "A") == ["p1", "p2"]
    assert GrafosV1.firing(red, marcado, "C") == ["p0"]
    assert GrafosV1.firing(red, ["p1", "p2"], "B") == ["p2", "p3"]
  end

  test "Pregunta 2: enablement con pares ordenados" do
    red = GrafosV1.pares1
    assert GrafosV1.enablement(red, ["p0"]) == ["A"]
    assert GrafosV1.enablement(red, ["p1", "p2"]) == ["B", "D", "C"]
    assert GrafosV1.enablement(red, ["p2", "p3"]) == ["C"]
  end

  test "Pregunta 3: replaying con pares ordenados" do
    red1 = GrafosV1.pares1
    red2 = GrafosV1.pares2
    marcado = ["p0"]
    archivo = "log1.txt"
    assert GrafosV1.replay(red1, marcado, archivo) == [2, 8]
    assert GrafosV1.replay(red2, marcado, archivo) == [6, 4]
  end

  test "Pregunta 4: grafo de alcance con pares ordenados" do
    red1 = GrafosV1.pares1
    red2 = GrafosV1.pares2
    red3 = GrafosV1.pares3
    marcado = ["p0"]
    assert GrafosV1.reachability_graph(red1, marcado, []) == [
      [["p0"], "A", ["p1", "p2"]],
      [["p1", "p2"], "B", ["p2", "p3"]],
      [["p2", "p3"], "C", ["p3", "p4"]],
      [["p3", "p4"], "E", ["p5"]],
      [["p1", "p2"], "D", ["p3", "p4"]],
      [["p1", "p2"], "C", ["p1", "p4"]],
      [["p1", "p4"], "B", ["p3", "p4"]]
    ]
    assert GrafosV1.reachability_graph(red2, marcado, []) == [
      [["p0"], "A", ["p1", "p2"]],
      [["p1", "p2"], "B", ["p2", "p3"]],
      [["p2", "p3"], "C", ["p3", "p4"]],
      [["p3", "p4"], "E", ["p5"]],
      [["p3", "p4"], "D", ["p2", "p3"]],
      [["p1", "p2"], "C", ["p1", "p4"]],
      [["p1", "p4"], "B", ["p3", "p4"]],
      [["p1", "p4"], "D", ["p1", "p2"]]
    ]
    assert GrafosV1.reachability_graph(red3, marcado, []) == [
      [["p0"], "A", ["p1", "p2"]],
      [["p1", "p2"], "B", ["p2", "p3"]],
      [["p2", "p3"], "C", ["p3", "p4"]],
      [["p3", "p4"], "E", ["p5"]],
      [["p1", "p2"], "D", ["p3"]],
      [["p1", "p2"], "C", ["p1", "p4"]],
      [["p1", "p4"], "B", ["p3", "p4"]]
    ]
  end
end

defmodule GrafosV2Test do
  use ExUnit.Case
  doctest GrafosV2

  test "Pregunta 1: firing con presets y posets" do
    red = GrafosV2.sets1
    marcado = ["p0"]
    assert GrafosV2.firing(red, marcado, :A) == ["p1", "p2"]
    assert GrafosV2.firing(red, marcado, :C) == ["p0"]
    assert GrafosV2.firing(red, ["p1", "p2"], :B) == ["p2", "p3"]
  end

  test "Pregunta 2: enablement con presets y posets" do
    red = GrafosV2.sets1
    assert GrafosV2.enablement(red, [:p0]) == ["A"]
    assert GrafosV2.enablement(red, [:p1, :p2]) == ["B", "D", "C"]
    assert GrafosV2.enablement(red, [:p2, :p3]) == ["C"]
  end

  test "Pregunta 3: replaying con presets y posets" do
    red1 = GrafosV2.sets1
    red2 = GrafosV2.sets2
    marcado = ["p0"]
    archivo = "log1.txt"
    assert GrafosV2.replay(red1, marcado, archivo) == [2, 8]
    assert GrafosV2.replay(red2, marcado, archivo) == [6, 4]
  end

  test "Pregunta 4: grafo de alcance con presets y posets" do
    red1 = GrafosV2.sets1
    red2 = GrafosV2.sets2
    red3 = GrafosV2.sets3
    marcado = ["p0"]
    assert GrafosV2.reachability_graph(red1, marcado, []) == [
      [["p0"], "A", ["p1", "p2"]],
      [["p1", "p2"], "B", ["p2", "p3"]],
      [["p2", "p3"], "C", ["p3", "p4"]],
      [["p3", "p4"], "E", ["p5"]],
      [["p1", "p2"], "D", ["p3", "p4"]],
      [["p1", "p2"], "C", ["p1", "p4"]],
      [["p1", "p4"], "B", ["p3", "p4"]]
    ]
    assert GrafosV2.reachability_graph(red2, marcado, []) == [
      [["p0"], "A", ["p1", "p2"]],
      [["p1", "p2"], "B", ["p2", "p3"]],
      [["p2", "p3"], "C", ["p3", "p4"]],
      [["p3", "p4"], "E", ["p5"]],
      [["p3", "p4"], "D", ["p2", "p3"]],
      [["p1", "p2"], "C", ["p1", "p4"]],
      [["p1", "p4"], "B", ["p3", "p4"]],
      [["p1", "p4"], "D", ["p1", "p2"]]
    ]
    assert GrafosV2.reachability_graph(red3, marcado, []) == [
      [["p0"], "A", ["p1", "p2"]],
      [["p1", "p2"], "B", ["p2", "p3"]],
      [["p2", "p3"], "C", ["p3", "p4"]],
      [["p3", "p4"], "E", ["p5"]],
      [["p1", "p2"], "D", ["p3"]],
      [["p1", "p2"], "C", ["p1", "p4"]],
      [["p1", "p4"], "B", ["p3", "p4"]]
    ]
  end
end
